Feature: LoginFeature
  This feature deals with the login functionality of the application

  Scenario: Login with correct usernama and password
    Given I navigate to the login page
    And I enter the user email address as Email: admin
    And I verify the count of my salary digits for Rs 1000
    And I enter the following for Login
      | userName | password      |
      | admin    | adminpassword |

    And i click login button
    Then I should see the user form page

  Scenario Outline: Login with correct usernama and password using Scenario outline
    Given I navigate to the login page
    And I enter <userName> and <password>
    And i click login button
    Then I should see the user form page

    Examples:
      | userName | password      |
      | execute  | adminpassword |
      | admin    | admin         |
      | testing  | qa            |