package Steps;

import Base.BaseUtil;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Hook extends BaseUtil {

    private BaseUtil base;

    public Hook(BaseUtil base) {
        this.base = base;
    }

    @Before
    public void Initializetest(){

        System.out.println("open brouser: firefox");

        System.setProperty("Webdriver.gecko.driver", "C:\\Users\\trucekt\\Downloads\\geckodriver-v0.23.0-win64\\geckodriver.exe");
        base.Driver = new FirefoxDriver();

    }

    @After
    public void TearDownTest(){
        System.out.println("close brouser");
    }
}
