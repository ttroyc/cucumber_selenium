package Steps;

import Base.BaseUtil;
import Transformation.SalaryCountTransformer;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.Transform;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import Transformation.EmailTransform;

import java.util.ArrayList;
import java.util.List;

public class LoginStep extends BaseUtil {

    private BaseUtil base;

    public LoginStep(BaseUtil base){
        this.base = base;

    }
    @Then("^I should see the user form page$")
    public void iShouldSeeTheUserFormPage() throws Throwable {
        System.out.println("I should see user form page");

    }

    @Given("^I navigate to the login page$")
    public void iNavigateToTheLoginPage() throws Throwable {
        System.out.println("Vidim prvu stranicu ");
        base.Driver.navigate().to("http://www.executeautomation.com/demosite/Login.html");

    }

    @And("^I enter the following for Login$")
    public void iEnterTheFollowingForLogin(DataTable table) throws Throwable {
    /*List<List<String>> data = table.raw();
    System.out.print("The value is : " + data.get(0).get(0).toString());
    System.out.print(" The value is : " + data.get(0).get(1).toString());*/

        List<User> users = new ArrayList<User>();
        users = table.asList(User.class);

        for (User user : users) {
            System.out.println("The Username is " + user.userName);
            System.out.println("The password is " + user.password);
        }
    }

    @And("^I enter ([^\"]*) and ([^\"]*)$")
    public void iEnterUserNameAndPassword(String userName, String password) throws Throwable {
        System.out.println("UserName is : " + userName);
        System.out.println("password is : " + password );
    }

    @And("^I enter the user email address as Email: ([^\"]*)$")
    public void iEnterTheUserEmailAddressAsEmailAdmin(@Transform(EmailTransform.class) String email) throws Throwable {

        System.out.println("The email address is " + email);

    }

    @And("^I verify the count of my salary digits for Rs (\\d+)$")
    public void iVerifyTheCountOfMySalaryDigitsForRs(@Transform(SalaryCountTransformer.class) int salary) throws Throwable {
        System.out.println("My salary digits count:  " + salary);
    }

    public class User {
            public String userName;
            public String password;

            public User(String userName, String password) {
                this.userName = userName;
                this.password = password;
            }
        }


    @And("^i click login button$")
    public void iClickLoginButton() throws Throwable {
        System.out.println("Click button");

    }


}
